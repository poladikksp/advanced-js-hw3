"use strict";
const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

//* Method 1
// const newClients = [...clients1, ...clients2].reduce((uniqueClients, client) => {
//   if (uniqueClients.includes(client)) {
//     return uniqueClients;
//   } else {
//     return [...uniqueClients, client];
//   }
// }, []);

//* Method 2
const newClients = [...clients1, ...clients2].filter((client, index, array) => {
   return index === array.indexOf(client);
})

console.log(newClients);
